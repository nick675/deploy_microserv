#!/bin/bash

echo --- Step 1. Getting repository
git clone https://github.com/microservices-demo/microservices-demo
cd microservices-demo

echo --- Step 2. Creating deployment
kubectl create -f deploy/kubernetes/manifests
