#!/bin/bash

# Обновляем пакетный менеджер apt
echo --Step 1 install apt-get--
sudo apt-get update

echo --Step 2 python2 to python 3--
sudo ln -sfn /usr/bin/python3 /usr/bin/python

# Устанавливаем python pip
echo --Step 3 install python-pip--
sudo apt-get install -y python-pip

# Устанавливаем ansible
echo --Step 4 install ansible--
sudo apt-get install -y ansible

# Устанавливаем OpenJDK
echo --Step 5 install openjdk--
sudo apt-get install -y default-jdk

# Устанавливаем wget and ufw
echo --Step 6 install wget, ufw, sshpass
sudo apt-get install -y wget
sudo apt-get install -y ufw
sudo apt-get install -y sshpass
export JAVA_OPTS=‘-Djenkins.install.runSetupWizard=false’
# Устанавливаем и запускаем Jenkins
echo --Step 7 install jenkins--
sudo wget -q -O - https://pkg.jenkins.io/debian-stable/jenkins.io.key | sudo apt-key add -
sudo sh -c 'echo deb https://pkg.jenkins.io/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list'
sudo apt-get update
sudo apt-get install -y jenkins
export JAVA_OPTS=‘-Djenkins.install.runSetupWizard=false’
sudo systemctl start jenkins
sudo service jenkins status
sudo ufw allow OpenSSH
sudo ufw allow 8080
sudo ufw status
sleep 30
# Создаем пользователя Jenkins и скачиваем Jenkins-CLI
  wget -P /var/tmp/devops http://localhost:8080/jnlpJars/jenkins-cli.jar && pass=`sudo cat /var/lib/jenkins/secrets/initialAdminPassword` && echo 'jenkins.model.Jenkins.instance.securityRealm.createAccount("nick1", "Password123")' | sudo java -jar /var/tmp/devops/jenkins-cli.jar -auth admin:$pass -s http://localhost:8080/ groovy =


/usr/bin/java -jar /var/tmp/devops/jenkins-cli.jar -auth nick1:Password123 -s http://localhost:8080 install-plugin ansible
/usr/bin/java -jar /var/tmp/devops/jenkins-cli.jar -auth nick1:Password123 -s http://localhost:8080 install-plugin timestamper
/usr/bin/java -jar /var/tmp/devops/jenkins-cli.jar -auth nick1:Password123 -s http://localhost:8080 install-plugin credentials-binding
/usr/bin/java -jar /var/tmp/devops/jenkins-cli.jar -auth nick1:Password123 -s http://localhost:8080 install-plugin build-timeout
/usr/bin/java -jar /var/tmp/devops/jenkins-cli.jar -auth nick1:Password123 -s http://localhost:8080 install-plugin workflow-aggregator
/usr/bin/java -jar /var/tmp/devops/jenkins-cli.jar -auth nick1:Password123 -s http://localhost:8080 install-plugin pipeline-stage-view
/usr/bin/java -jar /var/tmp/devops/jenkins-cli.jar -auth nick1:Password123 -s http://localhost:8080 install-plugin ssh-slaves
/usr/bin/java -jar /var/tmp/devops/jenkins-cli.jar -auth nick1:Password123 -s http://localhost:8080 install-plugin pipeline-github-lib
/usr/bin/java -jar /var/tmp/devops/jenkins-cli.jar -auth nick1:Password123 -s http://localhost:8080 install-plugin matrix-auth
/usr/bin/java -jar /var/tmp/devops/jenkins-cli.jar -auth nick1:Password123 -s http://localhost:8080 install-plugin gradle
/usr/bin/java -jar /var/tmp/devops/jenkins-cli.jar -auth nick1:Password123 -s http://localhost:8080 install-plugin git -restart

#service jenkins restart
sleep 40
# Запускаем Jenkins-CLI
echo --Step 9 Run Jenkins-CLI
java -jar /var/tmp/devops/jenkins-cli.jar -auth nick1:Password123 -s http://localhost:8080 who-am-i

# Создаем xml файлы для pipeline Jenkins
echo --Step 10 Create xml fr Jenkins Pipeline
cd /var/tmp/devops
## Pipeline delete_all
echo '<?xml version='"'1.1'"' encoding='"'UTF-8'"'?>
<flow-definition plugin="workflow-job@2.41">
  <actions>
    <org.jenkinsci.plugins.pipeline.modeldefinition.actions.DeclarativeJobAction plugin="pipeline-model-definition@1.9.2"/>
    <org.jenkinsci.plugins.pipeline.modeldefinition.actions.DeclarativeJobPropertyTrackerAction plugin="pipeline-model-definition@1.9.2">
      <jobProperties/>
      <triggers/>
      <parameters/>
      <options/>
    </org.jenkinsci.plugins.pipeline.modeldefinition.actions.DeclarativeJobPropertyTrackerAction>
  </actions>
  <description></description>
  <keepDependencies>false</keepDependencies>
  <properties/>
  <definition class="org.jenkinsci.plugins.workflow.cps.CpsFlowDefinition" plugin="workflow-cps@2.94">
    <script>pipeline {
    agent any

    stages {
        stage (&apos;Git clone&apos;) {
            steps {
                git &apos;https://gitlab.com/nick675/deploy_microserv.git&apos;
                }
            }
        stage (&apos;delete application&apos;) {
            steps {
                ansiblePlaybook credentialsId: &apos;20&apos;, disableHostKeyChecking: true,
                installation: &apos;ansible&apos;, inventory: &apos;hosts&apos;, playbook: &apos;delete_app.yaml&apos;
            }
        }
    }
}</script>
    <sandbox>true</sandbox>
  </definition>
  <triggers/>
  <disabled>false</disabled>
</flow-definition>' > delete_app.xml

## Pipeline deploy_all
echo '<?xml version='"'1.1'"' encoding='"'UTF-8'"'?>
<flow-definition plugin="workflow-job@2.41">
  <actions>
    <org.jenkinsci.plugins.pipeline.modeldefinition.actions.DeclarativeJobAction plugin="pipeline-model-definition@1.9.2"/>
    <org.jenkinsci.plugins.pipeline.modeldefinition.actions.DeclarativeJobPropertyTrackerAction plugin="pipeline-model-definition@1.9.2">
      <jobProperties/>
      <triggers/>
      <parameters/>
      <options/>
    </org.jenkinsci.plugins.pipeline.modeldefinition.actions.DeclarativeJobPropertyTrackerAction>
  </actions>
  <description></description>
  <keepDependencies>false</keepDependencies>
  <properties/>
  <definition class="org.jenkinsci.plugins.workflow.cps.CpsFlowDefinition" plugin="workflow-cps@2.94">
    <script>pipeline {
    agent any
    
    stages {
        stage (&apos;Git clone&apos;) {
            steps {
                git &apos;https://gitlab.com/nick675/deploy_microserv.git&apos;
                }
            }
        stage (&apos;Deploy minikube&apos;) {
            steps {
                ansiblePlaybook credentialsId: &apos;20&apos;, disableHostKeyChecking: true, 
                installation: &apos;ansible&apos;, inventory: &apos;hosts&apos;, playbook: &apos;playbook_minikube.yaml&apos;
            }
        }
        stage (&apos;Deploy microserv&apos;) {
            steps {
                ansiblePlaybook credentialsId: &apos;20&apos;, disableHostKeyChecking: true, 
                installation: &apos;ansible&apos;, inventory: &apos;hosts&apos;, playbook: &apos;playbook_microserv.yaml&apos;
            }
        }
    }
}</script>
    <sandbox>true</sandbox>
  </definition>
  <triggers/>
  <disabled>false</disabled>
</flow-definition>' > deploy_all.xml

## Pipeline deploy_app
echo '<?xml version='"'1.1'"' encoding='"'UTF-8'"'?>
<flow-definition plugin="workflow-job@2.41">
  <actions>
    <org.jenkinsci.plugins.pipeline.modeldefinition.actions.DeclarativeJobAction plugin="pipeline-model-definition@1.9.2"/>
    <org.jenkinsci.plugins.pipeline.modeldefinition.actions.DeclarativeJobPropertyTrackerAction plugin="pipeline-model-definition@1.9.2">
      <jobProperties/>
      <triggers/>
      <parameters/>
      <options/>
    </org.jenkinsci.plugins.pipeline.modeldefinition.actions.DeclarativeJobPropertyTrackerAction>
  </actions>
  <description></description>
  <keepDependencies>false</keepDependencies>
  <properties/>
  <definition class="org.jenkinsci.plugins.workflow.cps.CpsFlowDefinition" plugin="workflow-cps@2.94">
    <script>pipeline {
    agent any
    
    stages {
        stage (&apos;Git clone&apos;) {
            steps {
                git &apos;https://gitlab.com/nick675/deploy_microserv.git&apos;
                }
            }
        stage (&apos;deploy application&apos;) {
            steps {
                ansiblePlaybook credentialsId: &apos;20&apos;, disableHostKeyChecking: true, 
                installation: &apos;ansible&apos;, inventory: &apos;hosts&apos;, playbook: &apos;deploy_app.yaml&apos;
            }
        }
    }
}</script>
    <sandbox>true</sandbox>
  </definition>
  <triggers/>
  <disabled>false</disabled>
</flow-definition>' > deploy_app.xml

## Pipeline port-forward_front-end
echo '<?xml version='"'1.1'"' encoding='"'UTF-8'"'?>
<flow-definition plugin="workflow-job@2.41">
  <actions>
    <org.jenkinsci.plugins.pipeline.modeldefinition.actions.DeclarativeJobAction plugin="pipeline-model-definition@1.9.2"/>
    <org.jenkinsci.plugins.pipeline.modeldefinition.actions.DeclarativeJobPropertyTrackerAction plugin="pipeline-model-definition@1.9.2">
      <jobProperties/>
      <triggers/>
      <parameters/>
      <options/>
    </org.jenkinsci.plugins.pipeline.modeldefinition.actions.DeclarativeJobPropertyTrackerAction>
  </actions>
  <description></description>
  <keepDependencies>false</keepDependencies>
  <properties/>
  <definition class="org.jenkinsci.plugins.workflow.cps.CpsFlowDefinition" plugin="workflow-cps@2.94">
    <script>pipeline {
    agent any
    
    stages {
        stage (&apos;Git clone&apos;) {
            steps {
                git &apos;https://gitlab.com/nick675/deploy_microserv.git&apos;
                }
            }
        stage (&apos;Port forward&apos;) {
            steps {
                ansiblePlaybook credentialsId: &apos;20&apos;, disableHostKeyChecking: true, 
                installation: &apos;ansible&apos;, inventory: &apos;hosts&apos;, playbook: &apos;playbook_frontend_port-forward.yaml&apos;
            }
        }
    }
}
</script>
    <sandbox>true</sandbox>
  </definition>
  <triggers/>
  <disabled>false</disabled>
</flow-definition>' > port-forward_front-end.xml

## Pipeline proxy_dashboard
echo '<?xml version='"'1.1'"' encoding='"'UTF-8'"'?>
<flow-definition plugin="workflow-job@2.41">
  <actions>
    <org.jenkinsci.plugins.pipeline.modeldefinition.actions.DeclarativeJobAction plugin="pipeline-model-definition@1.9.2"/>
    <org.jenkinsci.plugins.pipeline.modeldefinition.actions.DeclarativeJobPropertyTrackerAction plugin="pipeline-model-definition@1.9.2">
      <jobProperties/>
      <triggers/>
      <parameters/>
      <options/>
    </org.jenkinsci.plugins.pipeline.modeldefinition.actions.DeclarativeJobPropertyTrackerAction>
  </actions>
  <description></description>
  <keepDependencies>false</keepDependencies>
  <properties/>
  <definition class="org.jenkinsci.plugins.workflow.cps.CpsFlowDefinition" plugin="workflow-cps@2.94">
    <script>pipeline {
    agent any
    
    stages {
        stage (&apos;Git clone&apos;) {
            steps {
                git &apos;https://gitlab.com/nick675/deploy_microserv.git&apos;
                }
            }
        stage (&apos;Kubernetes dashboard&apos;) {
            steps {
                ansiblePlaybook credentialsId: &apos;20&apos;, disableHostKeyChecking: true, 
                installation: &apos;ansible&apos;, inventory: &apos;hosts&apos;, playbook: &apos;playbook_kuber_dashboard.yaml&apos;
            }
        }
    }
}</script>
    <sandbox>true</sandbox>
  </definition>
  <triggers/>
  <disabled>false</disabled>
</flow-definition>' > proxy_dashboard.xml



sudo chmod a+x delete_app.xml
sudo chmod a+x deploy_all.xml
sudo chmod a+x deploy_app.xml
sudo chmod a+x port-forward_front-end.xml
sudo chmod a+x proxy_dashboard.xml


# Создаем pipeline Jenkins
echo --Step 11 Create Jenkins Pipeline
sudo java -jar /var/tmp/devops/jenkins-cli.jar -auth nick1:Password123 -s http://localhost:8080 create-job deploy_all < deploy_all.xml
java -jar /var/tmp/devops/jenkins-cli.jar -auth nick1:Password123 -s http://localhost:8080 create-job delete_app < delete_app.xml
java -jar /var/tmp/devops/jenkins-cli.jar -auth nick1:Password123 -s http://localhost:8080 create-job deploy_app < deploy_app.xml
java -jar /var/tmp/devops/jenkins-cli.jar -auth nick1:Password123 -s http://localhost:8080 create-job port-forward_front-end < port-forward_front-end.xml
java -jar /var/tmp/devops/jenkins-cli.jar -auth nick1:Password123 -s http://localhost:8080 create-job proxy_dashboard < proxy_dashboard.xml

# Устанавливаем плагин Ansible
#echo --Step 12 Install Ansible plugin
#java -jar /usr/bin/jenkins-cli.jar -auth nick1:Password123 -s http://localhost:8080 install-plugin ansible


# Создаем credentials
echo --Step 13 Create credential
echo ' <com.cloudbees.plugins.credentials.impl.UsernamePasswordCredentialsImpl>
        <scope>GLOBAL</scope>
        <id>20</id>
        <description></description>
        <username>nick2</username>
        <password>Password123</password>
        <usernameSecret>false</usernameSecret>
      </com.cloudbees.plugins.credentials.impl.UsernamePasswordCredentialsImpl>' > credential.xml

chmod a+x credential.xml
java -jar /var/tmp/devops/jenkins-cli.jar -auth nick1:Password123 -s http://localhost:8080 create-credentials-by-xml system::system::jenkins _ < credential.xml

sudo service jenkins restart 


