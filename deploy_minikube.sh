#!/bin/bash

echo --- Step 2. Installing minikube
curl -Lo minikube https://storage.googleapis.com/minikube/releases/v1.22.0/minikube-linux-amd64
sudo chmod +x minikube
sudo mv minikube /usr/local/bin/


## install kubectl
echo --- Step 3. Installing kubectl
curl -Lo kubectl https://storage.googleapis.com/kubernetes-release/release/v1.21.0/bin/linux/amd64/kubectl
chmod +x kubectl && sudo cp kubectl /usr/local/bin/ && rm kubectl


## start minikube
echo --- Step 4. Starting minikube
minikube start --driver=docker --memory 8098 --addons dashboard metrics-server default-storageclass storage-provisioner

## enable dashboard
echo --- Step 5. Enable minikube dashboard

minikube addons enable dashboard
